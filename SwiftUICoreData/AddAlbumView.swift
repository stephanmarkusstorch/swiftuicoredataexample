//
//  AddAlbumView.swift
//  SwiftUICoreData
//
//  Created by Stephan Storch on 17.02.20.
//  Copyright © 2020 STEPHANSTORCH. All rights reserved.
//

import SwiftUI

struct AddAlbumView: View {
    
    @Environment(\.managedObjectContext) var moc
    @Environment(\.presentationMode) var presentationMode
    
    @State private var title = ""
    @State private var artist = ""
    
    var body: some View {
        
        NavigationView {
            
            Form {
                
                Section {
                    
                    TextField("Title", text: $title)
                    TextField("Artist / Singer / Band", text: $artist)
                }
                
                Section {
                    
                    Button("Save") {
                        
                        let album = Album(context: self.moc)
                        
                        album.title = self.title != "" ? self.title : nil
                        album.artist = self.artist != "" ? self.artist : nil
                        
                        try? self.moc.save()
                        
                        self.presentationMode.wrappedValue.dismiss()
                        
                    }
                    
                }
                
            }.navigationBarTitle("Add an Album")
        }
        
    }
}

struct AddAlbumView_Previews: PreviewProvider {
    static var previews: some View {
        AddAlbumView()
    }
}
