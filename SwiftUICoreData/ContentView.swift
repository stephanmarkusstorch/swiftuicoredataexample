//
//  ContentView.swift
//  SwiftUICoreData
//
//  Created by Stephan Storch on 16.02.20.
//  Copyright © 2020 STEPHANSTORCH. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @Environment(\.managedObjectContext) var moc
    
    @FetchRequest(entity: Album.entity(),
                  sortDescriptors:
        [NSSortDescriptor(keyPath: \Album.artist, ascending: true),
         NSSortDescriptor(keyPath: \Album.title, ascending: true)
        ],
                  predicate: NSPredicate(format: "title != %@", "")
    )
    var albums: FetchedResults<Album>

    @State private var showingAddScreen = false
    
    var body: some View {
        
        NavigationView {
            
            List {

                ForEach(albums, id: \.self) { album in

                    Text("\(album.title ?? "Unknown Title"), \(album.artist ?? "Unknown Artist / Singer / Band")")

                }.onDelete { (indexSet) in

                    for index in indexSet {

                        let album = self.albums[index]
                        self.moc.delete(album)

                        try? self.moc.save()

                    }

                }

            }
            .navigationBarTitle("My Favorite Music")
            .navigationBarItems(
                leading: EditButton(),
                trailing:
                Button(action: {
                    
                    self.showingAddScreen.toggle()
                    
                }) {
                    
                    Image(systemName: "plus")
                    
            })
            .sheet(isPresented: $showingAddScreen) {
                    
                AddAlbumView().environment(\.managedObjectContext, self.moc)
                    
            }
        }
        
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
